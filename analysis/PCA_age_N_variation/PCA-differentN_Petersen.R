library(dplyr)
library(ggplot2)
library(geomtextpath )
library(readODS)
library(tidyr)
library(stringr)
library(psych)
library(mice)
library(performance)
library(forcats)
library(relimp, pos = 4)
library(paran)
library(factoextra)

#Analyse PCA results for different N with 
# MNI WMH based on fsaverage MNI Bullseye segmentation, calculated by M. Petersen
# ~40.000 individuals

res_UKBB_cross=read.csv("/data/pt_life_whm/Results/multivariate_cSVD/bullseye/UKBB/res_UKBB_cross.csv")

##to have same order as for other BE results
res_long_true=res_UKBB_cross%>%
  pivot_longer(cols=2:37, names_to = "bullseye_c", values_to="WMH")
regions=factor(rep(c("BG", "frontal_lh", "occipital_lh", "temporal_lh", "parietal_lh", "frontal_rh", "occipital_rh", "temporal_rh", "parietal_rh"),
                   each=4),
               levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                          "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                          "temporal_lh", "frontal_lh"))
depth=rep(c("1","2","3","4"),9)
res_long_true$region=rep(regions,length(unique(res_long_true$subj)))
res_long_true$depth=rep(depth,length(unique(res_long_true$subj)))
colnames(res_long_true)[1]=c("pseudonym")



res_df_sum = res_long_true %>% group_by(pseudonym) %>% 
  summarize(WMH_sum = sum(WMH, na.rm=TRUE)) %>% 
  mutate(asinhWMH = asinh(WMH_sum))


#MERGE with demographic data
#res=merge(res, sample[c("pseudonym", "Age_all", "sex", "waist2hip", "hypertension_composite_score", "EstimatedTotalIntraCranialVol")], by.x="subj", by.y="pseudonym")

#The individual parcellations are adjusted for TIV by the formula from the paper
#meanTIV=mean(sample$EstimatedTotalIntraCranialVol)
#res = res %>% mutate(WMH_vol_adj=WMH_vol*meanTIV/EstimatedTotalIntraCranialVol)

##TRY PCA
source("/data/pt_life_whm/Analysis/multivariate_risk_svd/LIFE/R/bullseye/create_bullseye_plot.R")
source("/data/pt_life_whm/Analysis/multivariate_risk_svd/LIFE/R/bullseye/create_PCA.R")

#for initalization
pca_UKBB=create_PCA(res_long_true, dep="WMH", method="pca",nfactors = 2, 
                    rotation="none")
fapar=fa.parallel(pca_UKBB[[1]],fa = "pc", n.obs=length(unique(res_long_true$pseudonym)))

retained_paran=list()
rot="oblimin"

for (i in c(1:10)){
for (N in c(2000, 5000, 10000, 20000, nrow(res_UKBB_cross))){ #)
  #for (rot in c("none", "oblimin")){
  
  #randomly select individuals from overall sample:
  subj_vec=c(1:nrow(res_UKBB_cross))
  subj_vec_tmp=sample(subj_vec, N, replace = FALSE)
  
  res_ana=res_UKBB_cross[subj_vec_tmp,]
  
  res_long_true=res_ana%>%
    pivot_longer(cols=2:37, names_to = "bullseye_c", values_to="WMH")
  regions=factor(rep(c("BG", "frontal_lh", "occipital_lh", "temporal_lh", "parietal_lh", "frontal_rh", "occipital_rh", "temporal_rh", "parietal_rh"),
                     each=4),
                 levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                            "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                            "temporal_lh", "frontal_lh"))
  depth=rep(c("1","2","3","4"),9)
  res_long_true$region=rep(regions,length(unique(res_long_true$subj)))
  res_long_true$depth=rep(depth,length(unique(res_long_true$subj)))
  colnames(res_long_true)[1]=c("pseudonym")
  
  #for creating corrmatrix to be used in fa.parallel
  pca_UKBB=create_PCA(res_long_true, dep="WMH", method="pca",nfactors = 2, #fapar$ncomp, 
                      rotation=rot)

  #check scree plot
  fapar=fa.parallel(pca_UKBB[[1]],fa = "pc", n.obs=length(unique(res_long_true$pseudonym)))

  #faparan=paran(pca_UKBB[[1]], iterations = 5000, centile = 0, quietly = FALSE, 
  #     status = TRUE, all = TRUE, cfa = TRUE, graph = TRUE, color = TRUE, 
  #      col = c("black", "red", "blue"), lty = c(1, 2, 3), lwd = 1, legend = TRUE, 
  #      file = "", width = 640, height = 640, grdevice = "png", seed = 0)
  
  #retained_paran=append(retained_paran, c(fapar$ncomp, faparan$Retained))
  
  #running actual PCA with number of components
  pca_UKBB=create_PCA(res_long_true, dep="WMH", method="pca",nfactors = fapar$ncomp, 
                      rotation=rot, ncol=ceiling(fapar$ncomp/2))
  
  pcaplot=  pca_UKBB[[3]] +
    theme_bw() +
    theme(panel.border = element_blank(),
          axis.text.x = element_text(size=8,
                                     colour = "black", vjust = 0.05),
          axis.title.x = element_blank(),
          axis.title.y = element_blank(),
          axis.ticks.y=element_blank(),
          axis.text.y = element_blank(),
          legend.position = "none",
          panel.grid.major = element_blank(),
          strip.text.x = element_text(size = 10,
                                      margin = margin( b = 1, t = 1)),
          panel.spacing.x = unit(0.5, "lines"),
          plot.margin=grid::unit(c(0,0,0,0), "mm"))
  # 
  ggsave(pcaplot,
        file=paste0("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Results/UKBB/figures/",
                    "Bullseye_UKBB_rot", rot, "_N",N,"_i_", i,"_comp_",fapar$ncomp,".png"),
        width = 10, height=10, unit="cm")
}}

#Use only those with highest WMH sum
subj_high_wmh=res_df_sum[res_df_sum$WMH_sum>quantile(res_df_sum$WMH_sum, 0.8),"pseudonym"]
df_high_wmh=res_UKBB_cross %>% filter(subj %in% subj_high_wmh$pseudonym)

res_long_true=df_high_wmh%>%
  pivot_longer(cols=2:37, names_to = "bullseye_c", values_to="WMH")
regions=factor(rep(c("BG", "frontal_lh", "occipital_lh", "temporal_lh", "parietal_lh", "frontal_rh", "occipital_rh", "temporal_rh", "parietal_rh"),
                   each=4),
               levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                          "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                          "temporal_lh", "frontal_lh"))
depth=rep(c("1","2","3","4"),9)
res_long_true$region=rep(regions,length(unique(res_long_true$subj)))
res_long_true$depth=rep(depth,length(unique(res_long_true$subj)))
colnames(res_long_true)[1]=c("pseudonym")

#for creating corrmatrix to be used in fa.parallel
pca_UKBB=create_PCA(res_long_true, dep="WMH", method="pca",nfactors = 3, 
                    rotation="oblimin", ncol=3)
fapar=fa.parallel(pca_UKBB[[1]],fa = "pc", n.obs=length(unique(res_long_true$pseudonym)))
#6 optimal number from fa.parallel

faparan=paran(pca_UKBB[[1]], iterations = 5000, centile = 0, quietly = FALSE, 
              status = TRUE, all = TRUE, cfa = TRUE, graph = TRUE, color = TRUE, 
              col = c("black", "red", "blue"), lty = c(1, 2, 3), lwd = 1, legend = TRUE, 
              file = "", width = 640, height = 640, grdevice = "png", seed = 0)
#3 from faparan
pca_UKBB=create_PCA(res_long_true, dep="WMH", method="pca",nfactors = 4, 
                    rotation="none", ncol=4)

pcaplot=  pca_UKBB[[3]] +
  theme_bw() +
  theme(panel.border = element_blank(),
        axis.text.x = element_text(size=8,
                                   colour = "black", vjust = 0.05),
        axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        axis.ticks.y=element_blank(),
        axis.text.y = element_blank(),
        legend.position = "none",
        panel.grid.major = element_blank(),
        strip.text.x = element_text(size = 10,
                                    margin = margin( b = 1, t = 1)),
        panel.spacing.x = unit(0.5, "lines"),
        plot.margin=grid::unit(c(0,0,0,0), "mm"))
# 
ggsave(pcaplot,
       file=paste0("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Results/UKBB/figures/",
                   "Bullseye_UKBB_rotoblimin_N7957_80percWMH_comp4.png"),
       width = 10, height=10, unit="cm")
