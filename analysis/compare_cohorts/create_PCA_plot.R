create_PCA_plot<-function(nfactors, type, data.princip){
  
  tmp=data.princip$loadings
  tmp2=as.data.frame(tmp[,c(seq(1:nfactors))])
  tmp2$regions=row.names(tmp2)
  
  l_oblimin=tmp2%>%
    pivot_longer(cols=seq(1:nfactors),names_to = "comp", values_to = "loadings")
  
  if (type=="hammers1"){
  print("hammers1")
  #add NA loadings to occipital_rh_1
  for (hemi in c("lh", "rh")){
    for (i in seq(1:nfactors)){
      l_oblimin = rbind(l_oblimin, c(paste0("occipital_",hemi,"_1"), paste0("TC",i), NA))
    }}
  
  l_oblimin$region=rep(factor(c("BG","BG","BG","BG", "frontal_lh", "frontal_lh", "frontal_lh", "frontal_lh",
                                "frontal_rh","frontal_rh","frontal_rh","frontal_rh", "occipital_lh","occipital_lh","occipital_lh",
                                "occipital_rh","occipital_rh","occipital_rh","parietal_lh","parietal_lh","parietal_lh","parietal_lh",
                                "parietal_rh","parietal_rh","parietal_rh","parietal_rh",
                                "temporal_lh","temporal_lh","temporal_lh","temporal_lh",
                                "temporal_rh","temporal_rh","temporal_rh","temporal_rh", 
                                "occipital_lh", "occipital_rh"),
                              levels = c("frontal_lh", "temporal_lh", "parietal_lh",
                                         "occipital_lh", "BG", "occipital_rh", "parietal_rh",
                                         "temporal_rh", "frontal_rh")),each=nfactors) 
  #rename regions for plotting
  l_oblimin$shell=rep(factor(c("1", "2", "3", "4",
                               "1", "2", "3", "4",
                               "1", "2", "3", "4",
                               "2", "3", "4", 
                               "2", "3", "4", 
                               "1", "2", "3", "4",
                               "1", "2", "3", "4",
                               "1", "2", "3", "4",
                               "1", "2", "3", "4", 
                               "1","1"), levels=c("1", "2", "3", "4")), each=nfactors)
  }else{
    print("hammers2")
    
    l_oblimin$region=rep(factor(c("BG","BG","BG","BG", 
                                  "frontal_lh", "frontal_lh", "frontal_lh", "frontal_lh",
                                  "frontal_rh","frontal_rh","frontal_rh","frontal_rh", 
                                  "occipital_lh","occipital_lh","occipital_lh","occipital_lh",
                                  "occipital_rh","occipital_rh","occipital_rh","occipital_rh",
                                  "parietal_lh","parietal_lh","parietal_lh","parietal_lh",
                                  "parietal_rh","parietal_rh","parietal_rh","parietal_rh",
                                  "temporal_lh","temporal_lh","temporal_lh","temporal_lh",
                                  "temporal_rh","temporal_rh","temporal_rh","temporal_rh"),
                                levels = c("frontal_lh", "temporal_lh", "parietal_lh",
                                           "occipital_lh", "BG", "occipital_rh", "parietal_rh",
                                           "temporal_rh", "frontal_rh")),each=nfactors) 
    #rename regions for plotting
    l_oblimin$shell=rep(factor(c("1", "2", "3", "4",
                                 "1", "2", "3", "4",
                                 "1", "2", "3", "4",
                                 "1", "2", "3", "4", 
                                 "1", "2", "3", "4", 
                                 "1", "2", "3", "4",
                                 "1", "2", "3", "4",
                                 "1", "2", "3", "4",
                                 "1", "2", "3", "4"), levels=c("1", "2", "3", "4")), each=nfactors)
  }
    
    
    varexpl=round(data.princip$Vaccounted["Proportion Explained",], 2)
    labnames=as_labeller(c("TC1" = paste0("TC1 (", varexpl["TC1"],")"),
                           "TC2" = paste0("TC2 (", varexpl["TC2"],")"),
                           "TC3" = paste0("TC3 (", varexpl["TC3"],")"),
                           "TC4" = paste0("TC4 (", varexpl["TC4"],")"),
                           "TC5" = paste0("TC5 (", varexpl["TC5"],")"),
                           "TC6" = paste0("TC6 (", varexpl["TC6"],")"),
                           "TC7" = paste0("TC7 (", varexpl["TC7"],")")))
    
    l_oblimin$loadings=as.numeric(l_oblimin$loadings)
    
    
  
  
    p2=ggplot(l_oblimin, aes(x = region, y =shell)) +
      geom_tile(aes(fill=loadings),
                color = "gray25",
                lwd = 0.4,
                linetype = 1)+
      coord_curvedpolar(theta= "x")+
      scale_fill_gradient(low = "white", high = "red") +
      #annotate(geom="text",label="City1", y=0.1, x=0.1)+
      facet_wrap(~comp,ncol = ceiling(nfactors/2), labeller = labnames) +
      theme_bw() +
      theme(panel.border = element_blank(),
            axis.text.x = element_text(size=8,
                                       colour = "black", vjust = 0.05),
            axis.title.x = element_blank(),
            axis.title.y = element_blank(),
            axis.ticks.y=element_blank(),
            axis.text.y = element_blank(),
            legend.position = "none",
            panel.grid.major = element_blank(),
            strip.text.x = element_text(size = 10,
                                        margin = margin( b = 1, t = 1)),
            panel.spacing.x = unit(0.5, "lines"),
            plot.margin=grid::unit(c(0,0,0,0), "mm"))
  
  
  return(p2)
}