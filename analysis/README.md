# analysis

## main
Main analysis scripts to create components from raw Bullseye segmentation.

### individual:

`create_PCA_UKBB_original.R`: script to generate PCA from individual space Bullseye WMH segmentation

(to be run with `./run_data_prep.sh` on cluster)

USES:

- `create_bullseye_df.R`: will extract BE data and merge into one file
- `create_PCA.R`: will create PCA and prepare object to be plotted


### MNI:
`create_PCA_UKBB_MNI.R`/`create_PCA_UKBB_MNI_hammersed.R`: scripts to generate PCA from MNI space Bullseye WMH segmentation

USES:

- `create_bullseye_df.R`: will extract BE data and merge into one file
- `create_PCA.R`: will create PCA and prepare object to be plotted
- `create_PCA_plot.R`: will create PCA and prepare object to be plotted depending on whether occipital 1. shell is left out ("Hammers2")

## compare cohorts
- `compare_individual_Hammers_UKBB.R`: compare PCA in individual and MNI Hammers space in UKBB
- `compare_PCA_UKBB_LIFE_MNI_hammersed.R`: compare PCA and Hammers edited in UKBB and LIFE
- `compare_PetersenWMH.R`: extract PCA for 40.000 UKB participants from M. Petersen but without demographics (only on MPI server)
- `compare_three_cohorts.R`: additionally compare 3C PCA, and merged files

## PCA_age_N_variation
- varying age and N in individual and Petersen data to see stability of PCA
