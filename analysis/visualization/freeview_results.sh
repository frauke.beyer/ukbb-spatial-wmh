
for sub in sub-1010698 sub-1032476
#changing from native to MNI space in occipital_rh_4: sub-1004979, sub-1010698
#fixing to y-axis: sub-1003174 #sub-1014748 sub-1015863 sub-1043877 sub-1000413 sub-1004011
#fixing to x axis sub-1104100 sub-1044520 sub-1162262 sub-3873662 sub-3888164
do
freeview /georges/ukbiobank/data/orig/bulk_data/$sub/ses-01/FreeSurfer/mri/T1.mgz /georges/ukbiobank/data/orig/bulk_data/$sub/ses-01/FreeSurfer/mri/aseg.mgz:colormap=lut /georges/ukbiobank/data/orig/bulk_data/$sub/ses-01/T2_FLAIR/T2_FLAIR_brain.nii.gz /scratch/fbeyer/output/$sub/bullseye_wmparc.nii.gz:colormap=lut /scratch/fbeyer/output/$sub/final_mask_warped.nii.gz:colormap=heat 


freeview /scratch/fbeyer/ukbb_mni/bullseyeWMparc_Hammers_MNIspace_34ROI_1mm.nii.gz /scratch/fbeyer/ukbb-spatial-wmh/bullseye_workflow/MNI152_T1_1mm.nii.gz /scratch/fbeyer/output/$sub/final_mask_MNI.nii.gz:colormap=heat
done
