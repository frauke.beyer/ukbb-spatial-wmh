#!/bin/bash
#SBATCH --job-name=Prep_R
#SBATCH --output=JOB_Prep_R-%j.out
#SBATCH --cpus-per-task=1


module load R/3.6.1 

R --vanilla << END
source('create_PCA_UKBB_original.R') 
END
#
