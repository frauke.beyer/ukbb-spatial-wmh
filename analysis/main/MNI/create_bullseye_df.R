create_bullseye<-function(location="/link/to/res.txt", pattern="res_sum.txt", scale_TIV=TRUE){
  
# Function to create Bullseye WM segmented WMH volume dataframe for further analysis
# Input: Location is path to the location where all results files are located (can be in subfolders)
# Possible patterns: res_sum.txt (WMH volume from individual BE segmentation)
#		     res_be_vol.txt (regional volume from individual BE segmentation
#		     volume_WMH.txt (volume of WMH voxels in parcel from MNI BE segmentation)
#		     _mean_WMHweight.txt (average of WMH probability in parcel from MNI BE segmentation)
  
files <- list.files(path=location, pattern=pattern, full.names=TRUE, recursive=TRUE)

if (grepl("res_sum.txt",pattern)){

i=0
for (file in files){
  print(file)
  if (i==0){
    res=read.csv(file, header=T)
    res=t(res)
    i=i+1}
  else{tmp=read.csv(file, header=T)
  res=rbind(res,t(tmp))
  
  }
}
  

  
res=as.data.frame(res)

colnames=c(
  "BG_1","BG_2", "BG_3", "BG_4",
  "frontal_lh_1","frontal_lh_2","frontal_lh_3","frontal_lh_4",
  "occipital_lh_1","occipital_lh_2","occipital_lh_3","occipital_lh_4",
  "temporal_lh_1","temporal_lh_2","temporal_lh_3","temporal_lh_4",
  "parietal_lh_1","parietal_lh_2","parietal_lh_3","parietal_lh_4",
  "frontal_rh_1","frontal_rh_2","frontal_rh_3","frontal_rh_4",
  "occipital_rh_1","occipital_rh_2","occipital_rh_3","occipital_rh_4",
  "temporal_rh_1","temporal_rh_2","temporal_rh_3","temporal_rh_4",
  "parietal_rh_1","parietal_rh_2","parietal_rh_3","parietal_rh_4",
  "CC_Posterior","CC_Mid_Posterior", "CC_Central", "CC_Mid_Anterior", "CC_Anterior", "eTIV")
  
  colnames(res)=colnames

## ADAPT to your subject IDs here
#res$pseudonym=str_replace(substring(rownames(res),first=4, last=14), '\\.', '-')
res$pseudonym=substring(rownames(res),first=4)


res_long = res %>% 
    pivot_longer(
      cols = BG_1:parietal_rh_4,
      names_to = c("bullseye_c"),
      values_to = "val",
      values_drop_na = TRUE
    )
  
  
  
regions=factor(rep(c("BG", "frontal_lh", "occipital_lh", "temporal_lh", "parietal_lh", "frontal_rh", "occipital_rh", "temporal_rh", "parietal_rh"),
                 each=4),
                 levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                            "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                            "temporal_lh", "frontal_lh"))
depth=rep(c("1","2","3","4"),9)
res_long$region=rep(regions,length(unique(res$pseudonym)))
res_long$depth=rep(depth,length(unique(res$pseudonym)))
  
#ONLY for WMH volumes: Adjust for TIV by scaling for ratio to mean TIV
if (scale_TIV){
  meanTIV=mean(res_long$eTIV, na.rm=T)
  res_long = res_long %>% mutate(WMH_vol_adj=val*meanTIV/eTIV)}
}else if(grepl("res_be_vol.txt",pattern)){

i=0
for (file in files){
  print(file)
  if (i==0){
    res=read.csv(file, header=T)
    res=t(res)
    i=i+1}
  else{tmp=read.csv(file, header=T)
  res=rbind(res,t(tmp))
  
  }
}
  

  
res=as.data.frame(res)

  colnames=c("BG_1","BG_2", "BG_3", "BG_4",
  "frontal_lh_1","frontal_lh_2","frontal_lh_3","frontal_lh_4",
  "occipital_lh_1","occipital_lh_2","occipital_lh_3","occipital_lh_4",
  "temporal_lh_1","temporal_lh_2","temporal_lh_3","temporal_lh_4",
  "parietal_lh_1","parietal_lh_2","parietal_lh_3","parietal_lh_4",
  "frontal_rh_1","frontal_rh_2","frontal_rh_3","frontal_rh_4",
  "occipital_rh_1","occipital_rh_2","occipital_rh_3","occipital_rh_4",
  "temporal_rh_1","temporal_rh_2","temporal_rh_3","temporal_rh_4",
  "parietal_rh_1","parietal_rh_2","parietal_rh_3","parietal_rh_4",
  "CC_Posterior","CC_Mid_Posterior", "CC_Central", "CC_Mid_Anterior", "CC_Anterior")
  
  
  colnames(res)=colnames

## ADAPT to your subject IDs here
#res$pseudonym=str_replace(substring(rownames(res),first=4, last=14), '\\.', '-')
res$pseudonym=substring(rownames(res),first=4)


res_long = res %>% 
    pivot_longer(
      cols = BG_1:parietal_rh_4,
      names_to = c("bullseye_c"),
      values_to = "val",
      values_drop_na = TRUE
    )
  
  
  
regions=factor(rep(c("BG", "frontal_lh", "occipital_lh", "temporal_lh", "parietal_lh", "frontal_rh", "occipital_rh", "temporal_rh", "parietal_rh"),
                 each=4),
                 levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                            "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                            "temporal_lh", "frontal_lh"))
depth=rep(c("1","2","3","4"),9)
res_long$region=rep(regions,length(unique(res$pseudonym)))
res_long$depth=rep(depth,length(unique(res$pseudonym)))
  
#ONLY for WMH volumes: Adjust for TIV by scaling for ratio to mean TIV
if (scale_TIV){
  meanTIV=mean(res_long$eTIV, na.rm=T)
  res_long = res_long %>% mutate(WMH_vol_adj=val*meanTIV/eTIV)}
}else if(grepl("_volume_WMH.txt",pattern)){

i=0
for (file in files){
  print(file)
  if (i==0){
    res=read.csv(file, header=F, sep=" ")
    res=t(res$V1) #only use average within WMH (-M)
    rownames(res)=substring(file, first=57, last=67)
    i=i+1}
  else{tmp=read.csv(file, header=F, sep=" ")
  tmp=t(tmp$V1)
  rownames(tmp)=substring(file, first=57, last=67)
  res=rbind(res,tmp)
  
  }
}
  

  
res=as.data.frame(res)
colnames=c(
  "BG_1","BG_2", "BG_3", "BG_4",
  "frontal_lh_1","frontal_lh_2","frontal_lh_3","frontal_lh_4",
  "frontal_rh_1","frontal_rh_2","frontal_rh_3","frontal_rh_4",
  "occipital_lh_1","occipital_lh_2","occipital_lh_3","occipital_lh_4",
  "occipital_rh_1", "occipital_rh_2","occipital_rh_3","occipital_rh_4",
  "parietal_lh_1","parietal_lh_2","parietal_lh_3","parietal_lh_4",
   "parietal_rh_1","parietal_rh_2","parietal_rh_3","parietal_rh_4",
  "temporal_lh_1","temporal_lh_2","temporal_lh_3","temporal_lh_4",
  "temporal_rh_1","temporal_rh_2","temporal_rh_3","temporal_rh_4")

colnames(res)=colnames

## ADAPT to your subject IDs here
#res$pseudonym=str_replace(substring(rownames(res),first=4, last=14), '\\.', '-')
res$pseudonym=rownames(res)


res_long = res %>% 
    pivot_longer(
      cols = BG_1:temporal_rh_4,
      names_to = c("bullseye_c"),
      values_to = "val",
      values_drop_na = TRUE
    )
  
  
  
regions=data.frame(region=factor(c("BG","BG","BG","BG", "frontal_lh", "frontal_lh", "frontal_lh", "frontal_lh",
"frontal_rh","frontal_rh","frontal_rh","frontal_rh", 
"occipital_lh","occipital_lh","occipital_lh","occipital_lh",
"occipital_rh","occipital_rh","occipital_rh","occipital_rh","parietal_lh","parietal_lh","parietal_lh","parietal_lh",
   "parietal_rh","parietal_rh","parietal_rh","parietal_rh",
  "temporal_lh","temporal_lh","temporal_lh","temporal_lh",
  "temporal_rh","temporal_rh","temporal_rh","temporal_rh"),
                      levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                                 "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                                 "temporal_lh", "frontal_lh")),
                    shell=factor(c("1", "2", "3", "4",
                    "1", "2", "3", "4",
                    "1", "2", "3", "4",
                    "1", "2", "3", "4",
                    "1", "2", "3",  "4",
"1", "2", "3", "4","1", "2", "3", "4","1", "2", "3", "4","1", "2", "3", "4"),
                     levels=c("1", "2", "3", "4")))

res_long$region=rep(regions$region,length(unique(res$pseudonym)))
res_long$depth=rep(regions$shell,length(unique(res$pseudonym)))

}else if(grepl("_mean_WMHweight.txt",pattern)){

i=0
for (file in files){
  print(file)
  if (i==0){
    res=read.csv(file, header=F, sep=" ")
    res=t(res$V1) #only use average within WMH (-M)
    rownames(res)=substring(file, first=57, last=67)
    i=i+1}
  else{tmp=read.csv(file, header=F, sep=" ")
  tmp=t(tmp$V1)
  rownames(tmp)=substring(file, first=57, last=67)
  res=rbind(res,tmp)
  
  }
}
  

  
res=as.data.frame(res)
colnames=c(
  "BG_1","BG_2", "BG_3", "BG_4",
  "frontal_lh_1","frontal_lh_2","frontal_lh_3","frontal_lh_4",
  "frontal_rh_1","frontal_rh_2","frontal_rh_3","frontal_rh_4",
  "occipital_lh_1","occipital_lh_2","occipital_lh_3","occipital_lh_4",
  "occipital_rh_1", "occipital_rh_2","occipital_rh_3","occipital_rh_4",
  "parietal_lh_1","parietal_lh_2","parietal_lh_3","parietal_lh_4",
   "parietal_rh_1","parietal_rh_2","parietal_rh_3","parietal_rh_4",
  "temporal_lh_1","temporal_lh_2","temporal_lh_3","temporal_lh_4",
  "temporal_rh_1","temporal_rh_2","temporal_rh_3","temporal_rh_4")

colnames(res)=colnames

## ADAPT to your subject IDs here
#res$pseudonym=str_replace(substring(rownames(res),first=4, last=14), '\\.', '-')
res$pseudonym=rownames(res)


res_long = res %>% 
    pivot_longer(
      cols = BG_1:temporal_rh_4,
      names_to = c("bullseye_c"),
      values_to = "val",
      values_drop_na = TRUE
    )
  
  
  
regions=data.frame(region=factor(c("BG","BG","BG","BG", "frontal_lh", "frontal_lh", "frontal_lh", "frontal_lh",
"frontal_rh","frontal_rh","frontal_rh","frontal_rh", 
"occipital_lh","occipital_lh","occipital_lh","occipital_lh",
"occipital_rh","occipital_rh","occipital_rh","occipital_rh","parietal_lh","parietal_lh","parietal_lh","parietal_lh",
   "parietal_rh","parietal_rh","parietal_rh","parietal_rh",
  "temporal_lh","temporal_lh","temporal_lh","temporal_lh",
  "temporal_rh","temporal_rh","temporal_rh","temporal_rh"),
                      levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                                 "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                                 "temporal_lh", "frontal_lh")),
                    shell=factor(c("1", "2", "3", "4",
                    "1", "2", "3", "4",
                    "1", "2", "3", "4",
                    "1", "2", "3", "4",
                    "1", "2", "3",  "4",
"1", "2", "3", "4","1", "2", "3", "4","1", "2", "3", "4","1", "2", "3", "4"),
                     levels=c("1", "2", "3", "4")))

res_long$region=rep(regions$region,length(unique(res$pseudonym)))
res_long$depth=rep(regions$shell,length(unique(res$pseudonym)))
  
}


  
return(list(res,res_long))
}
