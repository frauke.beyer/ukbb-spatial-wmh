create_PCA<-function(res_long, dep="WMH_vol_adj", method="pca", nfactors, rotation="oblimin", ncol=2){
  
#res_long should have columns WMH_vol_adj, bullseye_c, region, depth
#nfactors: number of factors in PCA  

res_long$asinhWMH = asinh(res_long[, dep])
rescross=res_long[, c("pseudonym","asinhWMH", "bullseye_c")]%>%
  pivot_wider(id_cols=pseudonym, names_from=bullseye_c, values_from=asinhWMH)
  
rescross=rescross%>%
  mutate_at(.vars = c(2:ncol(rescross)), ~(scale(.) %>% as.vector))
  
corr_matrix <- cor(rescross[,c(2:ncol(rescross))],use = "complete.obs")

#Using rescross (original input so that scores are returned)
if (method=="pca"){
data.princip=psych::principal(rescross[,c(2:ncol(rescross))], 
                              nfactors=nfactors, 
                              scores=TRUE,
                              rotate=rotation)
}else if (method=="fa"){
data.princip=fa(rescross[,c(2:ncol(rescross))],
                nfactors=nfactors, 
                n.obs = length(unique(res_long$pseudonym)),
                n.iter=1, 
                rotate=rotation, 
                scores="regression")}



return(list(corr_matrix,data.princip))
}
