# Installation of neurodocker -> https://www.repronim.org/neurodocker/user_guide/quickstart.html
miniconda3

conda create -n neurodocker python=3.9
conda activate neurodocker
python -m pip install neurodocker
neurodocker --help

source activate neurodocker

ssh comps08h04
cd /tmp/neurodocker

# Generate singularity image
neurodocker generate singularity \
    --pkg-manager apt \
    --base-image neurodebian:bullseye\
    --freesurfer version=7.3.0 \
    --miniconda version=latest conda_install="python==3.10.13 nipype==1.8.6 nibabel==5.2.0 numpy=1.26.3 pandas==2.1.4" \
    --user nonroot > becon
     
    
# building a new image using the Singularity file

export SINGULARITY_TMPDIR=/tmp/TMP
export SINGULARITY_CACHEDIR=/tmp/CACHE

singularity build --fakeroot bullseye.sif becon


Container located here because of huge size: /data/pt_life/bullseye.sif (4.5 GB)

