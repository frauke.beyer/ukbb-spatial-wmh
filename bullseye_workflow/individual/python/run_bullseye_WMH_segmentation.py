#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
running bullseye segmentation for a list of subjects
prerequisites:
	- freesurfer
	- flair/t2 and wmh probability maps
"""

from nipype import Node, Workflow, Function, SelectFiles
from nipype.interfaces import fsl
from nipype.interfaces.utility import IdentityInterface
import nipype.interfaces.freesurfer as fs
import nipype.interfaces.utility as util
import nipype.interfaces.io as nio    
from bullseye_pipeline import create_bullseye_pipeline
from create_flairreg_pipeline import create_flairreg_pipeline
from utils import extract_parcellation
import numpy as np 
import nibabel as nb
import os
import sys



def create_bullseye_lesion(subject, base_dir, freesurfer_dir, flair_file, wmh_file):
    """
    a workflow to extract wmh in bullseye segmented wm
    """

    # main workflow
    bullseye_lesion = Workflow(name="bullseyelesion_bbreg")
    bullseye_lesion.base_dir=base_dir+'/'+subject
    
    # bullseye wm segmentation part    
    bullseye=create_bullseye_pipeline()
    bullseye.inputs.inputnode.scans_dir=freesurfer_dir
    bullseye.inputs.inputnode.subject_id=subject

    # wmh registration to freesurfer
    lesionreg=create_flairreg_pipeline()
    lesionreg.inputs.inputnode.subject_id=subject
    lesionreg.inputs.inputnode.FLAIR=flair_file
    lesionreg.inputs.inputnode.LESION=wmh_file


    # extract wmh volumes from be segmentation 
    extractparc=Node(interface=util.Function(input_names=['in1_file', 'in2_file', 'subject_id', 'option'], output_names=['out_file', 'out_be_vol'],
                                               function=extract_parcellation), name='extractparc')
    extractparc.inputs.option="sum" 
    extractparc.inputs.subject_id=subject                                 
    #extractparc.inputs.in1_file=wmh_file
    
    # generate datasink
    datasink=Node(name="datasink", interface=nio.DataSink())
    datasink.inputs.base_directory = base_dir
    datasink.inputs.container=subject
    datasink.inputs.substitutions = [('_subject_', '')]

    # connect all nodes
    bullseye_lesion.connect([
        (bullseye, extractparc, [( 'outputnode.out_bullseye_wmparc', 'in2_file')]),
        (bullseye, lesionreg, [( 'outputnode.BRAIN', 'inputnode.BRAIN')]),
        (lesionreg, extractparc, [('outputnode.lesion2anat', 'in1_file')]),
        (bullseye, datasink,[( 'outputnode.out_bullseye_wmparc', '@bullseye')]),
        (lesionreg, datasink,[( 'outputnode.lesion2anat', '@lesion2anat')]),
        (lesionreg, datasink,[( 'outputnode.flair2anat', '@flair2anat')]),
        (extractparc, datasink,[( 'out_file', '@lesionparc')]),
        (extractparc, datasink,[( 'out_be_vol', '@bevolparc')])
    ])
   
    
    
    return bullseye_lesion



subject=sys.argv[1] 
base_dir=sys.argv[2] 
freesurfer_dir=sys.argv[3] 
flair_file=sys.argv[4]  
wmh_file=sys.argv[5] 

# run workflow in single-thread mode
bullseye_lesion=create_bullseye_lesion(subject, base_dir, freesurfer_dir, flair_file, wmh_file)
#bullseye_lesion.write_graph(graph2use='colored', simple_form=True)
bullseye_lesion.run()




