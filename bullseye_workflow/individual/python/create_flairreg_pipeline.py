from __future__ import division

import nipype.pipeline.engine as pe
import nipype.interfaces.utility as util
from nipype.interfaces.freesurfer import ApplyVolTransform, BBRegister
from nipype.interfaces.fsl import ImageStats

from nipype import IdentityInterface, DataSink

# from .utils import *
from utils import *

import os

def create_flairreg_pipeline():

    flairreg = pe.Workflow(name='flairreg_pipeline')
   
    inputnode = pe.Node(interface=IdentityInterface(fields=['subject_id', 'BRAIN', 'FLAIR', 'LESION']), 
                        name='inputnode')
    
    #don't need to use bbregister because FLAIR/Lesions are already in T1 space
    #enough to apply native to FREESURFER transform
    #see: https://biobank.ctsu.ox.ac.uk/crystal/crystal/docs/brain_mri.pdf

    #bbreg = pe.Node(BBRegister(contrast_type='t2',
    #                           out_fsl_file='flair2anat.mat',
    #                           out_reg_file='flair2anat.dat',
    #                           registered_file='flair2anat_bbreg.nii.gz',
    #                           ),
    #                           name='bbregister')

    #def create_list(in1,in2):
    #    return list(in1,in2)

    #mk_list=pe.Node(interface=util.Function(input_names=['in1', 'in2'], output_names=['out_file'],
    #                                               function=create_list), name='mk_list')
   
        
    # apply transform to lesionmap
    applyreg = pe.Node(ApplyVolTransform(), name="applyreg")
    applyreg.inputs.reg_header=True

    # for testing purpose included the option to also transform the FLAIR, but otherwise not needed
    applyreg2 = pe.Node(ApplyVolTransform(), name="applyreg2")
    applyreg2.inputs.reg_header=True

    # outputnode
    outputnode = pe.Node(IdentityInterface(fields=[
        'lesion2anat', 
        'flair2anat' 
        ]),
        name='outputnode')


    ##### CONNECTIONS #####
    flairreg.connect(inputnode     , 'LESION',        applyreg, 'source_file') 
    flairreg.connect(inputnode     , 'BRAIN',        applyreg, 'target_file') 
    flairreg.connect(applyreg     , 'transformed_file',         outputnode,  'lesion2anat') 
    flairreg.connect(inputnode     , 'FLAIR',        applyreg2, 'source_file') 
    flairreg.connect(inputnode     , 'BRAIN',        applyreg2, 'target_file') 
    flairreg.connect(applyreg2     , 'transformed_file',         outputnode,  'flair2anat') 


    return(flairreg)
