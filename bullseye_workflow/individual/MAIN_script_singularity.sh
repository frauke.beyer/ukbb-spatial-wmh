#!/bin/bash
#how to start singularity

#module load singularity/3.7.3
#

PATH_SCRIPT=/scratch/fbeyer/ukbb-spatial-wmh/bullseye_workflow/
PATH_OUT=/scratch/fbeyer/output
PATH_DATA=/scratch/fbeyer/input/
PATH_FREESURFER=${PATH_DATA}/Freesurfer
SUBJECTS_DIR=${PATH_FREESURFER}

for sj_id in sub-2105589 sub-4787009; # sj_id;
do

FLAIR=${PATH_DATA}/${sj_id}/T2_FLAIR.nii.gz #FLAIR image
LESION=${PATH_DATA}/${sj_id}/final_mask.nii.gz #LESION map

singularity exec --env SUBJECTS_DIR=${PATH_FREESURFER} --bind license.txt:/opt/freesurfer-7.3.0/.license --bind /scratch:/scratch --bind /georges/ukbiobank/data/orig/bulk_data bullseye.sif python $PATH_SCRIPT/python/run_bullseye_WMH_segmentation.py ${sj_id} $PATH_OUT $PATH_FREESURFER $FLAIR $LESION

done #< $PATH_SCRIPT/my_sublist.txt
