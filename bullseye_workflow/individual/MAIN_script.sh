#!/bin/bash
# Modify/add the following parameters as needed 

#SBATCH -J spatialwmh
#SBATCH -t 02:00:00
#SBATCH --output=spatialwmh_%A_%a.out
#SBATCH --error=spatialwmh_%A_%a.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=2500 #following https://www.mail-archive.com/freesurfer@nmr.mgh.harvard.edu/msg57788.html
#SBATCH --mail-user fbeyer@cbs.mpg.de
#SBATCH --mail-type ARRAY_TASKS,FAIL

# This is a script to run 
# FLAIR to T1 registration using FreeSurfer's bbregister
# Bullseye WM segmentation,
# extraction of 36 (Bullseye regions) + 5 (corpus callosum) spatial WMH volumes and
# extraction of TIV from FreeSurfer.

module load singularity/3.7.3

source ./array
sj_id=${FILES[$SLURM_ARRAY_TASK_ID]}


#sj_id=$5
PATH_SCRIPT=$1
PATH_OUT=$2
PATH_DATA=$3
PATH_FREESURFER=${PATH_DATA}/Freesurfer

###################################################################################################
#####										              #####
#####					PATHS THAT MUST BE CHANGED			      #####
#####											      #####
###################################################################################################				        

# File patterns to get native FLAIR/WMH maps for each subjct:	               
FLAIR=${PATH_DATA}/${sj_id}/T2_FLAIR.nii.gz #FLAIR image
LESION=${PATH_DATA}/${sj_id}/final_mask.nii.gz #LESION map

###################################################################################################
# Set FreeSurfer SUBJECTS_DIR directory -> needs to be done inside singularity
export SUBJECTS_DIR=${PATH_FREESURFER}

###################################################################################################
# Run Bullseye WM segmentation and WMH calculation
#use singularity to execute the python file with the appropriate files
cd ${PATH_SCRIPT}
#singularity exec bullseye.sif python --version

singularity exec --env SUBJECTS_DIR=${PATH_FREESURFER} --bind license.txt:/opt/freesurfer-7.3.0/.license --bind /scratch:/scratch --bind /georges/ukbiobank/data/orig/bulk_data bullseye.sif python $PATH_SCRIPT/python/run_bullseye_WMH_segmentation.py ${sj_id} $PATH_OUT $PATH_FREESURFER $FLAIR $LESION

###################################################################################################
# Copy EstimatedTotalIntraCranialVolume into results file
cat $PATH_FREESURFER/${sj_id}/stats/aseg.stats | grep EstimatedTotalIntraCranialVol |  awk -F, '{ print $4 }' >> $PATH_OUT/${sj_id}/res_sum.txt

###################################################################################################
# Removing intermediate files 

#rm -rf $PATH_OUT/${sj_id}/bullseyelesion_bbreg
