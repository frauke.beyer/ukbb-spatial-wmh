#!/bin/bash
# Command to launch the phenotype computation script for the spatial WMH GWAS project:
# ./{PATH to your SPATIALWMH_GWAS_COMPUTE_PHENOTYPES_scripts folder}/MAIN_script_batch.sh
# After modifying the paths in the first section of this script and in MAIN_script.sh, 
# it can be run as follows:
# ./{PATH to your SPATIALWMH_GWAS_COMPUTE_PHENOTYPES script folder}/MAIN_script_batch.sh

#############################################################################################################
#####													#####
#####					PATHS THAT MUST BE CHANGED					#####
#####													#####
#############################################################################################################

# Path to the provided directory /SPATIALWMH_GWAS_COMPUTE_PHENOTYPES_scripts/:
PATH_SCRIPT=/scratch/fbeyer/ukbb-spatial-wmh/bullseye_workflow/

# Path to the log output: defaults inside /SPATIALWMH_GWAS_COMPUTE_PHENOTYPES_scripts/ but can be changed
log_out=${PATH_SCRIPT}/log

# Number of jobs (subjects) that should be run simultaneously
JOB_ARR_LIMIT=100

# Path to the subjects' list:
sublist_name=${PATH_SCRIPT}/test.txt

# Path to output folder
PATH_OUT=/scratch/fbeyer/output

# Path to input dir with native FLAIR and WMH probability maps
PATH_DATA=/scratch/fbeyer/input/

#############################################################################################################

# Create and go to batch_output folder so that out/err go there
if [[ ! -d ${log_out} ]]; then
   mkdir -p ${log_out}
fi

cd $log_out

FILES=($(cat ${sublist_name}))
declare -p FILES > array
 
SIZE=${#FILES[@]}
ZBSIZE=$(($SIZE - 1))

# Create an job aray with sub-jobs for each subj.
echo "Spawning ${SIZE} sub-jobs."
 
if [ $ZBSIZE -ge 0 ]; then
sbatch --array=0-$ZBSIZE%${JOB_ARR_LIMIT} ${PATH_SCRIPT}/MAIN_script.sh ${PATH_SCRIPT} ${PATH_OUT} ${PATH_DATA} 
fi

