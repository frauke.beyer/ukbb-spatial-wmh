#!/bin/bash

#!/bin/bash
# Modify/add the following parameters as needed 

#SBATCH -J wmhmni
#SBATCH -t 02:00:00
#SBATCH --output=wmhmni%A_%a.out
#SBATCH --error=wmhmni%A_%a.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=2500 #following https://www.mail-archive.com/freesurfer@nmr.mgh.harvard.edu/msg57788.html
#SBATCH --mail-user fbeyer@cbs.mpg.de
#SBATCH --mail-type ARRAY_TASKS


source ./array
subj=${FILES[$SLURM_ARRAY_TASK_ID]}

MNImask="/scratch/fbeyer/ukbb_mni/bullseyeWMparc_Hammers_edited_MNIspace_36ROI_1mm.nii.gz" 
wmh_file="/scratch/fbeyer/output/$subj/final_mask_MNI.nii.gz"

if [ -f $wmh_file ] && [ ! -f "/scratch/fbeyer/output/$subj/${subj}_hammersed_mean_WMHweight.txt" ];
then 
echo $subj>> /scratch/fbeyer/ukbb-spatial-wmh/done_hammersed_extract_MNI.txt

#Average of WMH > 0
fslstats -K ${MNImask} $wmh_file -M -m >> /scratch/fbeyer/output/$subj/${subj}_hammersed_mean_WMHweight.txt

#Volume of WMH > 0
fslstats -K ${MNImask} $wmh_file -V >> /scratch/fbeyer/output/$subj/${subj}_hammersed_volume_WMH.txt

fi


