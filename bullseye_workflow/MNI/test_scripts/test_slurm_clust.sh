#!/bin/bash
#SBATCH -J singularity_test
#SBATCH -o singularity_test.out
#SBATCH -e singularity_test.err
#SBATCH -p test
#SBATCH -t 0-00:30
#SBATCH -c 1
#SBATCH --mem=4000

# Singularity command line options
singularity exec bullseye.sif cat /etc/os-release