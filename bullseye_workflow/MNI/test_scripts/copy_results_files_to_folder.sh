
cd /scratch/fbeyer/output/

for subj in `ls -d sub-*`
do
echo $subj

if [ ! -f intermediate/${subj}_res_sum.txt ]; 
then
cp $subj/res_sum.txt intermediate/${subj}_res_sum.txt
else 
echo "copied already"
fi

if [ ! -f intermediate/${subj}_be_vol.txt ]; 
then
cp $subj/res_be_vol.txt intermediate/${subj}_res_be_vol.txt
else 
echo "copied already"
fi

done
