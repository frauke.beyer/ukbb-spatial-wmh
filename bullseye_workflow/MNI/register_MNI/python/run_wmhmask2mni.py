#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Run WMH mask to MNI space transform
"""

from nipype import MapNode, Node, Workflow
from nipype.interfaces import fsl
import nipype.interfaces.io as nio    
import sys


def create_wmh2mni(subject, base_dir, flair_file, wmh_file, coeffs):
    """
    a workflow to transform wmh mask to MNI space
    """

    # main workflow
    wmh2mni = Workflow(name="wmh2mni")
    wmh2mni.base_dir=base_dir+'/'+subject
    
    # applying the warp to the images (without premat)+ iterfield just for testing     
    #applyreg=MapNode(fsl.ApplyWarp(), name="applyreg", iterfield='in_file')
    #applyreg.inputs.in_file = [flair_file, wmh_file]
    #applyreg.inputs.ref_file = '/scratch/fbeyer/ukbb-spatial-wmh/bullseye_workflow/MNI152_T1_1mm.nii.gz'
    #applyreg.inputs.field_file = coeffs 

    applyreg=Node(fsl.ApplyWarp(), name="applyreg")
    applyreg.inputs.in_file = wmh_file
    applyreg.inputs.ref_file = '/scratch/fbeyer/ukbb-spatial-wmh/bullseye_workflow/MNI152_T1_1mm.nii.gz'
    applyreg.inputs.field_file = coeffs 
    applyreg.inputs.out_file='final_mask_MNI.nii.gz'

    
    # generate datasink
    datasink=Node(name="datasink", interface=nio.DataSink())
    datasink.inputs.base_directory = base_dir
    datasink.inputs.container=subject
    datasink.inputs.substitutions = [('_subject_', '')]

    # connect all nodes
    wmh2mni.connect([
        (applyreg, datasink,[( 'out_file', '@mask2anat')])
    ])
   
    
    
    return wmh2mni



subject=sys.argv[1] 
base_dir=sys.argv[2] 
flair_file=sys.argv[3]  
wmh_file=sys.argv[4] 
coeffs=sys.argv[5] 

# run workflow in single-thread mode
wmh2mni=create_wmh2mni(subject, base_dir, flair_file, wmh_file, coeffs)
#wmh2mni.write_graph(graph2use='colored', simple_form=True)
wmh2mni.run()