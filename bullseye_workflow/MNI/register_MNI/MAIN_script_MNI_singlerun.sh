#!/bin/bash
#how to start singularity

module load fsl/6.0.4
#

PATH_SCRIPT=/scratch/fbeyer/ukbb-spatial-wmh/bullseye_workflow/
PATH_OUT=/scratch/fbeyer/output
PATH_DATA=/georges/ukbiobank/data/orig/bulk_data

for sj_id in sub-4787009; # sj_id = sub-4787009;
do

FLAIR=${PATH_DATA}/${sj_id}/ses-01/T2_FLAIR/T2_FLAIR_brain.nii.gz #T2 FLAIR
LESION=${PATH_DATA}/${sj_id}/ses-01/T2_FLAIR/lesions/final_mask.nii.gz #WMH mask
PREMAT=${PATH_DATA}/${sj_id}/ses-01/T1/transforms/T1_to_MNI_linear.mat #T1 2 mni linear
COEFFS=${PATH_DATA}/${sj_id}/ses-01/T1/transforms/T1_to_MNI_warp_coef.nii.gz #T1 2 mni warp
REF=${PATH_SCRIPT}/MNI152_T1_1mm.nii.gz
OUT_FLAIR=${PATH_OUT}/${sj_id}/FLAIR_MNI.nii.gz


#python $PATH_SCRIPT/python/run_wmhmask2mni.py ${sj_id} $PATH_OUT $FLAIR $LESION $COEFFS

applywarp -i $FLAIR -o $OUT_FLAIR -r $REF -w $COEFFS


done #< $PATH_SCRIPT/my_sublist.txt
