mri_vol2vol --mov /scratch/fbeyer/output/sub-1044520/bullseye_wmparc.nii.gz --targ /scratch/fbeyer/output/sub-1044520//001.mgz --regheader --o /scratch/fbeyer/output/sub-1044520/bullseye_wmparc_native.nii.gz --no-save-reg


COEFFS="/georges/ukbiobank/data/orig/bulk_data/sub-1044520/ses-01/T1/transforms/T1_to_MNI_warp_coef.nii.gz"
IN='/scratch/fbeyer/output/sub-1044520/bullseye_wmparc_native.nii.gz'
OUT='/scratch/fbeyer/output/sub-1044520/bullseye_wmparc_MNI.nii.gz'
REF='/scratch/fbeyer/ukbb-spatial-wmh/bullseye_workflow/MNI152_T1_1mm.nii.gz'


applywarp -i $IN -o $OUT -r $REF -w $COEFFS --interp nn
