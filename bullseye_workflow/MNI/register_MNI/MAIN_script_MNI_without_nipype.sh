#!/bin/bash
# Modify/add the following parameters as needed 

#SBATCH -J spatialwmh
#SBATCH -t 02:00:00
#SBATCH --output=spatialwmh_%A_%a.out
#SBATCH --error=spatialwmh_%A_%a.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=2500 #following https://www.mail-archive.com/freesurfer@nmr.mgh.harvard.edu/msg57788.html
#SBATCH --mail-user fbeyer@cbs.mpg.de
#SBATCH --mail-type ARRAY_TASKS,FAIL

# This is a script to run 
# 

module load fsl/6.0.4

source ./array
sj_id=${FILES[$SLURM_ARRAY_TASK_ID]}


PATH_SCRIPT=$1
PATH_OUT=$2
PATH_DATA=$3

###################################################################################################
#####										              #####
#####					PATHS THAT MUST BE CHANGED			      #####
#####											      #####
###################################################################################################				        
# File patterns to transform FLAIR/WMH maps for each subjct:	               
FLAIR=${PATH_DATA}/${sj_id}/ses-01/T2_FLAIR/T2_FLAIR_brain.nii.gz #T2 FLAIR
LESION=${PATH_DATA}/${sj_id}/ses-01/T2_FLAIR/lesions/final_mask.nii.gz #WMH mask
COEFFS=${PATH_DATA}/${sj_id}/ses-01/T1/transforms/T1_to_MNI_warp_coef.nii.gz #T1 2 mni warp
OUT=${PATH_OUT}/${sj_id}/final_mask_MNI.nii.gz
OUT_FLAIR=${PATH_OUT}/${sj_id}/FLAIR_MNI.nii.gz
REF=${PATH_SCRIPT}/bullseye_workflow/MNI152_T1_1mm.nii.gz

###################################################################################################

# Run applywarp to MNI for Lesions only
applywarp -i $LESION -o $OUT -r $REF -w $COEFFS

#To test FLAIR registration:
#applywarp -i $FLAIR -o $OUT_FLAIR -r $REF -w $COEFFS
###################################################################################################

